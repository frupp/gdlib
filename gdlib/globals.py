'''
This is just a namespace accessible via gdlib.globals
where arbitrary global variables can be stored.

It is empty by design.
'''
