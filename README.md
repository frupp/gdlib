# gdlib

This is a game development library using PyGame for the software development course @HdM.

## Gitlab CI/CD

To use CI/CD in gitlab it must be enabled in settings/CI/CD.

Pipeline Status: [![pipeline status](https://gitlab.com/frupp/gdlib/badges/main/pipeline.svg)](https://gitlab.com/frupp/gdlib/-/commits/main)